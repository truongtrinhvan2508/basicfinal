//
//  ServerConnection.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import Foundation

protocol ServerConnectionProtocol {
    func fetchAPIFromURL(_ url: String, complitionHandler: @escaping (String?, String?) -> Void) -> Void
}

class ServerConnection {
    
}

extension ServerConnection: ServerConnectionProtocol {
    
    /// Fetch API from url
    /// - Parameters:
    ///   - url: url
    ///   - complitionHandler: callback when fetch sucess or errro
    ///        (data, errormessage)
    func fetchAPIFromURL(_ url: String, complitionHandler: @escaping (String?, String?) -> Void) -> Void {
        guard let url = URL(string: url) else {
            complitionHandler(nil, "URL incorrect")
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error as? String {
                complitionHandler(nil, error)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    complitionHandler(nil, "HTTP status failed")
                return
            }
            
            guard let _data = data else {
                return
            }
            
            let string = String(data: _data, encoding: .utf8)
            
            complitionHandler(string, nil)
        }
        task.resume()
    }
}

