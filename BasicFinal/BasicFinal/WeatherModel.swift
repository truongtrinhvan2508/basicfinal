//
//  WeatherModel.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import Foundation

struct WeatherCity: Codable{
    
    let city: City
    let list :[List]
    
    enum WeatherCityCodingKeys: String, CodingKey{
        case city
        case list
    }
}

struct City: Codable {
    let name: String

    enum CityDetailKeys: String, CodingKey {
        case name
    }
}

struct List: Codable {
    let dt_txt: String
    let main: Main
    let weather: [Weather]
    let wind: Wind
    
    enum ListDetailKeys: String, CodingKey{
        case dt_txt = "dt_txt"
        case main
        case weather
        case wind
    }
}

struct Main: Codable{
    let temp :Double
    let temp_max: Double
    let temp_min:Double
    let pressure: Int
    let humidity:Int
    

    enum MainDetailKeys: String, CodingKey {
        case temp
        case temp_min = "temp_min"
        case temp_max = "temp_max"
        case pressure
        case humidity
    }
    
}

struct Wind: Codable{
    let speed: Float

    enum WindDetailKeys: String ,CodingKey{
        case speed
    }
}

struct Weather: Codable{
    let main: String
    let icon: String
    
    enum WeatherDetailKeys: String ,CodingKey{
        case main
        case icon
    }
}
