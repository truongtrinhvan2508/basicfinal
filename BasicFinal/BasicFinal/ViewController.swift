//
//  ViewController.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import UIKit

final class ViewController: UIViewController {
    var city:String = "Hanoi"
    private var ListWeather: WeatherCity?
    @IBOutlet weak var windSpeedLb: UILabel!
    @IBOutlet weak var tempMinLb: UILabel!
    @IBOutlet weak var tempMaxlb: UILabel!
    @IBOutlet weak var humidtyLb: UILabel!
    @IBOutlet weak var tempLb: UILabel!
    @IBOutlet weak var cityLb: UILabel!
    private let serverConnection = ServerConnection()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        fetchListWeather()
        self.title = "Weather"
    }

    @IBAction func invokeForecast(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "ForecastViewController") as? ForecastViewController {
            vc.title = "Forecast"
            vc.ListWeatherCity = ListWeather
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func invokeSetting(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController {
            vc.title = "Setting"
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension ViewController {
    private func fetchListWeather() {
        serverConnection.fetchAPIFromURL("https://pro.openweathermap.org/data/2.5/forecast/hourly?q=\(city)&appid=b1b15e88fa797225412429c1c50c122a1&fbclid=IwAR1DweJ8daHlGiF3wBoAH8QxBbeQ9F9zXHn-88Tfut94RflLMYQymXAzD_c")  { [weak self] (body, errorMessage) in
            
            guard let self = self else {
                print("Self released")
                return
            }
            
            if let errorMessage = errorMessage {
                print(errorMessage)
                // show error message
                return
            }
                        
            if let body = body {
                self.convertData(body)
            }
        }
    }
    
    private func convertData(_ data: String) {
        let data = Data(data.utf8)
        let decoder = JSONDecoder()
        // Decode json with dictionary
        let vaporEntity = try? decoder.decode(WeatherCity.self, from: data)
        self.ListWeather = vaporEntity!
        if ListWeather != nil{
            
        }
        if let weatherEntity = vaporEntity {
            DispatchQueue.main.async { [weak self] in
                self?.cityLb.text = weatherEntity.city.name
                self?.tempLb.text = "Temp: \(self?.formatData(weatherEntity.list[0].main.temp) ?? "")"
                self?.tempMinLb.text = "Temp Min: \(self?.formatData(weatherEntity.list[0].main.temp_min) ?? "")"
                self?.tempMaxlb.text = "Temp Max: \(self?.formatData(weatherEntity.list[0].main.temp_max) ?? "")"
                self?.humidtyLb.text = "Humidity: \(String(weatherEntity.list[0].main.humidity))"
                self?.windSpeedLb.text = "Wind speed: \(String(weatherEntity.list[0].wind.speed))"
            }
        }
    }
    
    func formatData(_ temp: Double) -> String{
        return "\(String(format: "%.0f", temp - 273.15))°C"
    }
}

extension ViewController: passData{
    func passDatacurrent(cityName: String) {
        self.city = cityName
        fetchListWeather() 
    }
    
    
}
