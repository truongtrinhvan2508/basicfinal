//
//  ForecastViewController.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import UIKit

class ForecastViewController: UIViewController {
    
    var ListWeatherCity: WeatherCity?
    @IBOutlet weak var ForecastTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView(){
        ForecastTableView.delegate = self
        ForecastTableView.dataSource = self
        ForecastTableView.register(UINib(nibName: "ForecastTableViewCell", bundle: nil), forCellReuseIdentifier: "ForecastTableViewCell")
    }

}

extension ForecastViewController: UITableViewDelegate{
    
}

extension ForecastViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListWeatherCity?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell", for: indexPath) as? ForecastTableViewCell else {
            return UITableViewCell()
        }
        cell.configueData(data: ListWeatherCity ?? WeatherCity(city: City(name: ""), list: []), index: indexPath.row)
        return cell
    }
    
    
}
