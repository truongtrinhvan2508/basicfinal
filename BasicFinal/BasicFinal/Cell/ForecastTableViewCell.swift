//
//  ForecastTableViewCell.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import UIKit

final class ForecastTableViewCell: UITableViewCell {

    @IBOutlet private weak var descriptionImage: UIImageView!
    @IBOutlet private weak var tempMaxLb: UILabel!
    @IBOutlet private weak var tempMinLb: UILabel!
    @IBOutlet private weak var dateLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configueData(data: WeatherCity, index: Int){
        dateLb.text = data.list[index].dt_txt
        tempMaxLb.text = "Temp Max: \(String(formatData(data.list[index].main.temp_max)))"
        tempMinLb.text = "Temp Min: \(String(formatData(data.list[index].main.temp_min)))"
        if data.list[index].weather.count > 0{
            descriptionImage.image = UIImage(named: data.list[index].weather[0].icon)
        }else{
            descriptionImage.image = UIImage(named: "")
        }
    }
      
    func formatData(_ temp: Double) -> String{
        return "\(String(format: "%.0f", temp - 273.15))°C"
    }
}
