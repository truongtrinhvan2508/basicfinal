//
//  SettingViewController.swift
//  BasicFinal
//
//  Created by TruongTV2 on 4/21/22.
//

import UIKit
protocol passData{
    func passDatacurrent(cityName: String)
}

class SettingViewController: UIViewController {

    @IBOutlet weak var currentCityTF: UITextField!
    var delegate:passData?
    override func viewDidLoad() {
        super.viewDidLoad()
         let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func invokePassDataCityName(_ sender: Any) {
        let cityName: String = currentCityTF.text ?? ""
        delegate?.passDatacurrent(cityName: cityName)
        navigationController?.popViewController(animated: true)
    }

}
